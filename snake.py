from tkinter import *
import random


game_width = 1000
game_height = 800
speed = 60
space_size = 30
snake_body = 3
snake_color = "#0000FF"
food_color = "#FFFF00"
background_color = "#000000"


class Snake:
    def __init__(self):
        self.body_size = snake_body
        self.coordinates = []
        self.squares = []
        # Create the snake position at beginning
        for i in range(0, snake_body):
            self.coordinates.append([0,0])
        # Create the square
        for x, y in self.coordinates:
            square = canvas.create_rectangle(x, y, x+space_size, y+space_size, fill=snake_color, tag="snake")
            self.squares.append(square)

class Food:
    def __init__(self):
        x = random.randint(0, int(game_width/space_size) -1) * space_size
        y = random.randint(0, int(game_height/space_size) -1) * space_size
        self.coordinates = [x,y]
        canvas.create_oval(x,y, x+space_size, y+space_size, fill=food_color, tag="food")

def next_turn(snake, food):
    x, y = snake.coordinates[0]
    if direction == "up":
        y -= space_size
    elif direction == "down":
        y += space_size
    elif direction == "left":
        x -= space_size
    elif direction == "right":
        x += space_size

    snake.coordinates.insert(0, (x,y))
    square = canvas.create_rectangle(x,y,x+space_size,y+space_size,fill=snake_color)
    # Each time the snake moves, we insert the square into the coordinates
    snake.squares.insert(0,square)

    # Everytime the snake eats the food, we delete and create new random food
    if x == food.coordinates[0] and y == food.coordinates[1]:
        global score
        score += 1
        label.config(text="Score:{}".format(score))
        canvas.delete("food")
        food = Food()
    else:
    # Delete the previous coordinates
        del snake.coordinates[-1]
        canvas.delete(snake.squares[-1])
        del snake.squares[-1]

    # The collisions if snake hits the border
    if check_collisions(snake):
        game_over()
    else:
        window.after(speed, next_turn, snake, food)

def change_direction(new_direction):
    global direction
    if new_direction == "left":
        if direction != "right":
            direction = new_direction
    elif new_direction == "right":
        if direction != "left":
            direction = new_direction
    elif new_direction == "up":
        if direction != "down":
            direction = new_direction
    elif new_direction == "down":
        if direction != "up":
            direction = new_direction

def check_collisions(snake):
    x, y = snake.coordinates[0]
    if x < 0 or x >= game_width:
        print("GAME OVER")
        return True
    elif y < 0 or y >= game_height:
        print("GAME OVER")
        return True
    for snake_body in snake.coordinates[1:]:
        if x == snake_body[0] and y == snake_body[1]:
            print("GAME OVER")
            return True
    return False


def game_over():
    canvas.delete(ALL)
    canvas.create_text(canvas.winfo_width()/2, canvas.winfo_height()/2, font=("consolas", 70), text="GAME OVER", fill="red", tag="gameover")

window = Tk()
window.title("SNAKE GAME")
window.resizable(False, False)

# Score table
score = 0
direction = "down"
label = Label(window, text="Score: {}".format(score), font=("consolas", 40))
label.pack()

# Game background
canvas = Canvas(window, bg=background_color, height=game_height, width=game_width)
canvas.pack()

# Center the window
window.update()
window_width = window.winfo_width()
window_height = window.winfo_height()
screen_width = window.winfo_screenwidth()
screen_height = window.winfo_screenheight()
x = int((screen_width/2) - (window_width/2))
y = int((screen_height/2) - (screen_height/2))

window.geometry(f"{window_width}x{window_height}+{x}+{y}")

# Bind the movement keys
window.bind("<Left>", lambda event: change_direction("left"))
window.bind("<Right>", lambda event: change_direction("right"))
window.bind("<Up>", lambda event: change_direction("up"))
window.bind("<Down>", lambda event: change_direction("down"))

# Snake
snake = Snake()
food = Food()
next_turn(snake,food)

window.mainloop()
